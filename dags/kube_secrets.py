from airflow.contrib.kubernetes.secret import Secret

PENDO_API_INTEGRATION_KEY = Secret(
    "env", "PENDO_API_INTEGRATION_KEY", "airflow-secrets", "PENDO_API_INTEGRATION_KEY"
)
