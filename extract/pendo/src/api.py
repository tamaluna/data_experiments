import os
import json

from datetime import datetime
from typing import Dict, Any

import requests


class PendoAPI:
    """
    Initialized with an integration_key, and optionally a
    start_date(2019-01-01), and request_interval(dayRange).
    Currently just allows fetching from the aggregation api.
    Currently returns a JSON datatype.
    """

    def __init__(
        self,
        integration_key=None,
        timeout=120
            ):

        self.base_url = "https://app.pendo.io/api/v1/"

        if integration_key is not None:
            self.integration_key = integration_key
        else:
            self.integration_key = os.environ.get("PENDO_API_INTEGRATION_KEY")
            if self.integration_key is None:
                raise Exception("Pendo integration key not configured")

        # Global headers
        self.headers = {}
        self.headers.update({"x-pendo-integration-key": self.integration_key})
        self.headers.update({'Cache-Control': "no-cache"})
        self.headers.update({"content-type": "application/json"})

        self.timeout = timeout



    def get_aggregation_json(self, query=None) -> Dict[Any, Any]:
        """
        API method to retrieve aggregation results based on a query.
        See ../extract/pendo/resources/requests/ for example queries.
        For instructions for how to build a query visit
        https://developers.pendo.io/docs/?python#aggregation
        json_query: a dictionary or string.
        """
        query = PendoQueryBuilder(query)

        url = self.base_url + "aggregation"

        self.data = json.dumps(query.query_dict)

        r = requests.post(
            url, headers=self.headers, data=self.data, timeout=self.timeout
            )

        r.raise_for_status()

        extract = r.json()
        aggregation_results = extract

        return aggregation_results


class PendoQueryBuilder():
    """
    A helper class to simplify modifying portions of a query.
    See ../extract/pendo/resources/requests/ for example queries.
    """

    def __init__(self, query=None, endpoint='aggregation'):
        self.query_dict = {}
        self.endpoint = endpoint

        if query is None:
            raise Exception("Please provide an aggregation query.\
            See ../extract/pendo/resources/requests/ for valid examples")
        else:

            try:
                if type(query) is not dict:
                    self.query_dict = json.loads(query)
                else:
                    self.query_dict = query
            except:
                raise Exception("'query' not recognized. \
                    See ../extract/pendo/resources/requests/ for valid examples")

    def get_source(self):
        try:
            source = list(self.query_dict['request']['pipeline'][0]['source'].keys())[0]
        except:
            raise Exception("source not found")

        return source


    def get_timeSeries(self):

        timeSeries=self.query_dict['request']['pipeline'][0]['source']\
            ['timeSeries']
        return timeSeries


    def update_timeSeries(self, period="dayRange", first="now()", count=1):

        try:
            self.query_dict['request']['pipeline'][0]['source']['timeSeries']\
                .update({'period': period, 'first': first, 'count': count})
        except:
            raise Exception("timeSeries object not found")

        return
